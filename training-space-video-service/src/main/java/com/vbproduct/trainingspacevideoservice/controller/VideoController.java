package com.vbproduct.trainingspacevideoservice.controller;

import com.vbproduct.trainingspacevideoservice.dto.VideoDto;
import com.vbproduct.trainingspacevideoservice.dto.convertor.DtoConverter;
import com.vbproduct.trainingspacevideoservice.service.VideoService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import static com.vbproduct.trainingspacevideoservice.constant.TrainingSpaceApiConstants.COURSE_ENDPOINT;
import static com.vbproduct.trainingspacevideoservice.constant.TrainingSpaceApiConstants.VIDEO;

@RestController
@CrossOrigin
@RequestMapping(COURSE_ENDPOINT)
public class VideoController {

    @Autowired
    private VideoService videoService;

    @ApiIgnore
    @GetMapping("/stream/{uid}")
    public ResponseEntity<StreamingResponseBody> getVideoStream(@PathVariable String uid) {
        StreamingResponseBody streamingResponseBody = (os) ->
                os.write(
                        FileUtils.openInputStream(videoService.getVideoFile(uid).getFile()).readAllBytes()
                );
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "video/MP2T")
                .header(HttpHeaders.ACCEPT_RANGES, "bytes")
                .body(streamingResponseBody);
    }

    @GetMapping(VIDEO + "/{uid}")
    public VideoDto getVideo(@PathVariable String uid) {
        return DtoConverter.convertToDto(videoService.getVideo(uid).get());
    }

    @GetMapping("/{courseUid}" + VIDEO)
    public List<VideoDto> getVideosByCourse(@PathVariable String courseUid) {
        return DtoConverter.convertToDtoList(videoService.getAllVideosByCourse(courseUid));
    }

    @GetMapping(VIDEO + "/file/{uid}")
    public ResponseEntity<Resource> getVideoFile(@PathVariable String uid) throws MalformedURLException {
        Resource file = videoService.getVideoFile(uid);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"")
                .header(HttpHeaders.CONTENT_TYPE, "video/mp4")
                .header(HttpHeaders.ACCEPT_RANGES, "bytes")
                .body(file);
    }

    @PostMapping(VIDEO + "/file")
    public VideoDto saveVideoFile(@RequestParam String userUid, @RequestParam String courseUid, @RequestParam("file") MultipartFile file) {
        return DtoConverter.convertToDto(videoService.saveVideoFile(userUid, courseUid, file));
    }

    @PutMapping(VIDEO + "/file")
    public void updateVideoFile(@RequestParam String videoUid, @RequestParam String userUid, @RequestParam String courseUid, @RequestParam("file") MultipartFile file) {
        videoService.updateVideoFile(videoUid, userUid, courseUid, file);
    }

    @DeleteMapping(VIDEO + "/{uid}")
    public void deleteVideo(@PathVariable String uid) {
        try {
            videoService.deleteVideo(uid);
        } catch (FileNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Video not found");
        }
    }

    @PutMapping(VIDEO)
    public VideoDto updateVideo(@RequestBody VideoDto videoDto) {
        var video = DtoConverter.convertToVideo(videoDto);
        return DtoConverter.convertToDto(videoService.updateVideo(video));
    }

    @DeleteMapping("{courseUid}" + VIDEO)
    public void deleteAllVideosByCourse(@PathVariable String courseUid) throws IOException {
        videoService.deleteAllVideosByCourse(courseUid);
    }

    @GetMapping(VIDEO + "/image/{videoUid}")
    public ResponseEntity<Resource> getVideoImage(@PathVariable String videoUid) throws IOException {
        var image = videoService.getImage(videoUid);
        return ResponseEntity.ok().body(image);
    }
}
