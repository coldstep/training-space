package com.vbproduct.trainingspacevideoservice.service.impl;

import com.vbproduct.trainingspacevideoservice.model.Video;
import com.vbproduct.trainingspacevideoservice.model.VideoSegment;
import com.vbproduct.trainingspacevideoservice.repository.VideoSegmentRepository;
import com.vbproduct.trainingspacevideoservice.service.VideoSegmentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
@Service
public class VideoSegmentServiceImpl implements VideoSegmentService {


    @Autowired
    private VideoSegmentRepository videoSegmentRepository;

    @Override
    public List<VideoSegment> getSegmentsFromVideo(Video video) {
        return videoSegmentRepository.findAllByVideo(video);
    }

    @Override
    public Resource getVideoSegment(long id) throws MalformedURLException {
        var videoSegment = videoSegmentRepository.findById(id);
        return new UrlResource(Paths.get(videoSegment.get().getFileUrl()).toUri());
    }

    @Override
    public void saveVideoSegment(VideoSegment videoSegment, MultipartFile file) {
        try {
            var videoFile = new File(videoSegment.getFileUrl());
            FileUtils.writeByteArrayToFile(videoFile, file.getInputStream().readAllBytes());
            videoSegmentRepository.save(videoSegment);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
    }

    @Transactional
    @Override
    public void deleteAllVideoSegmentsByVideo(Video video) {
        var segments = videoSegmentRepository.findAllByVideo(video);
        var url = segments.get(0).getFileUrl().split(video.getUid());
        var dirUrl = url[0] + video.getUid();
        try {
            FileUtils.deleteDirectory(new File(dirUrl));
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
        videoSegmentRepository.deleteAllByVideo(video);
    }

}
