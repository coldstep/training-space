package com.vbproduct.trainingspacevideoservice.service;


import com.vbproduct.trainingspacevideoservice.model.Video;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;

public interface VideoService {

    List<Video> getAllVideosByCourse(String courseUid);

    Optional<Video> getVideo(String videoUid);

    Resource getVideoFile(String videoUid) throws MalformedURLException;

    Video saveVideoFile(String userUid, String courseUid, MultipartFile file);

    void updateVideoFile(String videoUid, String userUid, String courseUid, MultipartFile file);

    void deleteVideo(String uid) throws FileNotFoundException;

    void deleteAllVideosByCourse(String courseUid) throws IOException;

    Video updateVideo(Video video);

    Resource getImage(String videoUid) throws IOException;
}
