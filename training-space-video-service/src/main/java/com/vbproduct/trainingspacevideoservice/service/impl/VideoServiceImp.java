package com.vbproduct.trainingspacevideoservice.service.impl;

import com.vbproduct.trainingspacevideoservice.model.Video;
import com.vbproduct.trainingspacevideoservice.model.VideoSegment;
import com.vbproduct.trainingspacevideoservice.model.VideoType;
import com.vbproduct.trainingspacevideoservice.repository.VideoRepository;
import com.vbproduct.trainingspacevideoservice.service.VideoSegmentService;
import com.vbproduct.trainingspacevideoservice.service.VideoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class VideoServiceImp implements VideoService {

    @Value("${training-space.dir-path}")
    private String dirPath;

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private VideoSegmentService videoSegmentService;

    @Override
    public List<Video> getAllVideosByCourse(String courseUid) {
        return videoRepository.findAllByCourseUid(courseUid);
    }

    @Override
    public Resource getVideoFile(String videoUid) throws MalformedURLException {
        var video = videoRepository.findById(videoUid)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Video not found"));
        var videoSegments = videoSegmentService.getSegmentsFromVideo(video);
        //TODO: in future this will be rewritten
        return videoSegmentService.getVideoSegment(videoSegments.get(0).getId());
    }

    @Override
    public Optional<Video> getVideo(String videoUid) {
        return videoRepository.findById(videoUid);
    }

    @Override
    public Video saveVideoFile(String userUid, String courseUid, MultipartFile file) {
        var video = new Video();
        video.setUid(UUID.randomUUID().toString());
        video.setCreatedDate(LocalDate.now());

        var fileNameParts = file.getOriginalFilename().split("\\.");
        video.setName(fileNameParts[0]);
        video.setVideoType(checkVideoType(fileNameParts[1]));
        video.setCourseUid(courseUid);
        video.setOwnerUid(userUid);
        //TODO how to set Duration ?
//        video.setDuration();

        video = videoRepository.save(video);
        video.setVideoSegments(saveSegments(video, file));
        return videoRepository.save(video);
    }

    @Override
    public void updateVideoFile(String videoUid, String userUid, String courseUid, MultipartFile file) {
        var video = getVideo(videoUid).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Video not found"));

        video.setCreatedDate(LocalDate.now());

        var fileNameParts = file.getOriginalFilename().split("\\.");
        video.setVideoType(checkVideoType(fileNameParts[1]));

        //TODO how to set Duration ?
//        video.setDuration();

        video = videoRepository.save(video);
        video.setVideoSegments(updateSegments(video, file));
        videoRepository.save(video);
    }

    @Override
    public void deleteVideo(String videoUid) throws FileNotFoundException {
        var video = videoRepository.findById(videoUid).orElseThrow(() -> new FileNotFoundException("File was not found"));
        videoSegmentService.deleteAllVideoSegmentsByVideo(video);
        videoRepository.delete(video);
    }

    @Transactional
    @Override
    public void deleteAllVideosByCourse(String courseUid) throws IOException {
        var video = videoRepository.findFirstByCourseUid(courseUid);
        var courseDir = video.getVideoSegments().get(0).getFileUrl().split(video.getUid())[0];
        FileUtils.deleteDirectory(new File(courseDir));
        var videos = getAllVideosByCourse(courseUid);

        videoRepository.deleteAllByCourseUid(courseUid);
    }

    @Override
    public Video updateVideo(Video video) {
        var storedVideo = videoRepository.findById(video.getUid()).orElseThrow();
        storedVideo.setName(video.getName());
        storedVideo.setDescription(video.getDescription());
        return videoRepository.save(storedVideo);
    }

    @Override
    public Resource getImage(String videoUid) throws IOException {
        var video = videoRepository.findById(videoUid).orElseThrow();
        return new UrlResource(Paths.get(video.getImage()).toUri());
    }

    private VideoType checkVideoType(String fileExtension) {
        switch (fileExtension) {
            case "mp4":
                return VideoType.MP4;
            case "webm":
                return VideoType.WEBM;
            default:
                return null;
        }
    }

    //TODO this method should be save file and return from db list
    private List<VideoSegment> saveSegments(Video video, MultipartFile file) {
        //TODO file name should be as segment uid + .mp4

        var userVideoDirectory = dirPath + video.getOwnerUid() + "/"
                + video.getCourseUid() + "/"
                + video.getUid() + "/";

        var videoSegment = new VideoSegment();
        videoSegment.setFileUrl(userVideoDirectory + file.getOriginalFilename());
        videoSegment.setVideo(video);
        videoSegmentService.saveVideoSegment(videoSegment, file);
        List list = new ArrayList<>();
        list.add(videoSegment);
        return list;
    }


    //TODO this method should be save file and return from db list
    private List<VideoSegment> updateSegments(Video video, MultipartFile file) {
        //TODO file name should be as segment uid + .mp4

        var userVideoDirectory = dirPath + video.getOwnerUid() + "/"
                + video.getCourseUid() + "/"
                + video.getUid() + "/";

        var videoSegment = new VideoSegment();
        videoSegment.setFileUrl(userVideoDirectory + file.getOriginalFilename());
        videoSegment.setVideo(video);
        videoSegmentService.deleteAllVideoSegmentsByVideo(video);
        videoSegmentService.saveVideoSegment(videoSegment, file);
        List list = new ArrayList<>();
        list.add(videoSegment);
        return list;
    }
}