package com.vbproduct.trainingspacevideoservice.service;

import com.vbproduct.trainingspacevideoservice.model.Video;
import com.vbproduct.trainingspacevideoservice.model.VideoSegment;
import jdk.dynalink.linker.LinkerServices;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.net.MalformedURLException;
import java.util.List;

public interface VideoSegmentService {

    List<VideoSegment> getSegmentsFromVideo(Video video);

    Resource getVideoSegment(long id) throws MalformedURLException;

    void saveVideoSegment(VideoSegment videoSegment, MultipartFile file);

    void deleteAllVideoSegmentsByVideo(Video video);

//    void updateVideoSegment(VideoSegment videoSegment, MultipartFile file);
}
