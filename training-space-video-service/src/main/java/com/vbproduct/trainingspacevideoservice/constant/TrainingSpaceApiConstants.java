package com.vbproduct.trainingspacevideoservice.constant;

public class TrainingSpaceApiConstants {
    public static final String API = "/api";
    public static final String COURSE_ENDPOINT = API+"/course";
    public static final String VIDEO = "/video";
    public static final String VIDEO_ENDPOINT = COURSE_ENDPOINT +"/video";
}
