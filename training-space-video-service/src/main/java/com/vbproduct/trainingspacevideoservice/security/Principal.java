package com.vbproduct.trainingspacevideoservice.security;

import javax.persistence.Id;

public class Principal {
    @Id
    private String uid;
    private String username;
    private String password;
}
