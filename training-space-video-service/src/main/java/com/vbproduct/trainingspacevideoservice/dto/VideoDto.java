package com.vbproduct.trainingspacevideoservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@NoArgsConstructor
public class VideoDto {
    private String uid;
    private String name;
    @JsonIgnore
    private List<String> videoSegmentUids;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String videoType;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String createdDate;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String duration;
    private String description;
    private String courseUid;
    private String owner;
}
