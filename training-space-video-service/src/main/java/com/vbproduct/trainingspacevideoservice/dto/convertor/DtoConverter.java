package com.vbproduct.trainingspacevideoservice.dto.convertor;

import com.vbproduct.trainingspacevideoservice.dto.VideoDto;
import com.vbproduct.trainingspacevideoservice.model.Video;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class DtoConverter {

    public static VideoDto convertToDto(Video video) {
        var videoDto = new VideoDto();
        videoDto.setUid(video.getUid());
        videoDto.setName(video.getName());
        videoDto.setVideoSegmentUids(
                video.getVideoSegments()
                        .stream()
                        .map(videoSegment -> Long.toString(videoSegment.getId()))
                        .collect(Collectors.toList())
        );
        videoDto.setCreatedDate(video.getCreatedDate().toString());
        videoDto.setDuration(video.getDuration());
        videoDto.setVideoType(video.getVideoType().toString());
        videoDto.setDescription(video.getDescription());
        videoDto.setCourseUid(video.getCourseUid());
        videoDto.setOwner(video.getOwnerUid());
        return videoDto;
    }

    public static Video convertToVideo(VideoDto videoDto) {
        var video = new Video();
        video.setUid(videoDto.getUid());
        video.setName(videoDto.getName());
        video.setDescription(videoDto.getDescription());
        return video;
    }

    public static List<VideoDto> convertToDtoList(List<Video> videos) {
        return videos.stream().map(DtoConverter::convertToDto).collect(Collectors.toList());
    }
}
