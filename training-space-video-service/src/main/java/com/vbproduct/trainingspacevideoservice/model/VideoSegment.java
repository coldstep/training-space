package com.vbproduct.trainingspacevideoservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "video_segment")
public class VideoSegment {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    @Column(name = "file_url",nullable = false)
    private String fileUrl;
    @ManyToOne
    @JoinColumn(name="video_uid")
    private Video video;
}
