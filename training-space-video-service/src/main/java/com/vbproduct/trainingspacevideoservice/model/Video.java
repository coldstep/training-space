package com.vbproduct.trainingspacevideoservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "video")
@NoArgsConstructor
public class Video {

    @Id
    private String uid;
    @Column(name = "name", nullable = false)
    private String name;
    @OneToMany(mappedBy = "video",cascade = CascadeType.REMOVE)
    private List<VideoSegment> videoSegments;
    @Enumerated(EnumType.STRING)
    @Column(name = "video_type")
    private VideoType videoType;
    @Column(name = "created_date")
    private LocalDate createdDate;
    @Column(name = "duration")
    private String duration;
    @Column(name = "description")
    private String description;
    @Column(name = "course_uid")
    private String courseUid;
    @Column(name = "owner_uid")
    private String ownerUid;
    @Column(name = "image_url")
    private String image;
}
