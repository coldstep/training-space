package com.vbproduct.trainingspacevideoservice.repository;


import com.vbproduct.trainingspacevideoservice.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VideoRepository extends JpaRepository<Video,String> {

    List<Video> findAllByCourseUid(String courseUid);

    Video findFirstByCourseUid(String courseUid);

    void deleteAllByCourseUid(String courseUid);
}
