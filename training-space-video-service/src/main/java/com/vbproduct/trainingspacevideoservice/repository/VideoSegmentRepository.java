package com.vbproduct.trainingspacevideoservice.repository;

import com.vbproduct.trainingspacevideoservice.model.Video;
import com.vbproduct.trainingspacevideoservice.model.VideoSegment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VideoSegmentRepository extends JpaRepository<VideoSegment, Long> {

    List<VideoSegment> findAllByVideo(Video video);

    void deleteAllByVideo(Video video);
}
