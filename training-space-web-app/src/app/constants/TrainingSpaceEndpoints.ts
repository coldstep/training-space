export class TrainingSpaceEndpoints {
  private static readonly PORT = '8080'
  static readonly SIGN_IN = `http://localhost:${TrainingSpaceEndpoints.PORT}/api/auth/signin`;
  static readonly SIGN_UP = `http://localhost:${TrainingSpaceEndpoints.PORT}/api/auth/signup`;
}
