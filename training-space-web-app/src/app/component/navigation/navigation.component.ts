import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from "../../service/auth/token-storage.service";
import {MediaUploadService} from "../../service/media/media-upload.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  private roles: string[];
  private authority: string;
  private image: any;
  private defaultImage: string = "../../assets/avatars/default/img_avatar.png";

  constructor(
    private tokenStorage: TokenStorageService,
    private uploadService: MediaUploadService) {
  }

  ngOnInit() {
    // if (this.tokenStorage.getToken()) {
    //   this.roles = this.tokenStorage.getAuthorities();
    //   this.roles.every(role => {
    //     if (role === 'ROLE_ADMIN') {
    //       this.authority = 'admin';
    //       return false;
    //     } else if (role === 'ROLE_PM') {
    //       this.authority = 'pm';
    //       return false;
    //     }
    //     this.authority = 'user';
    //     return true;
    //   });
    // }
    this.getUserImage();
  }

  openNav() {
    document.getElementById("main").style.opacity = '0.9';
    document.getElementById("main").style.pointerEvents = "none";
    document.getElementById("mySidebar").style.width = "300px";
  }

  closeNav() {
    document.getElementById("main").style.opacity = '1';
    document.getElementById("main").style.pointerEvents = "auto";
    document.getElementById("mySidebar").style.width = "0px";
  }

  getUserImage() {
    this.uploadService.getImage(this.tokenStorage.getKey()).subscribe(
      data => {
        this.image = data
      },
      error => {
        console.log(error)
      }
    );
  }


  logout() {
    this.tokenStorage.signOut();
    this.closeNav();
  }

}
