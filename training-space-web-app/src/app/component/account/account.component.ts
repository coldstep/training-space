import {Component, OnInit} from '@angular/core';
import {BasicService} from "../../service/basic.service";
import {TokenStorageService} from "../../service/auth/token-storage.service";
import {User} from "../../model/User";
import {MediaUploadService} from "../../service/media/media-upload.service";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {


  constructor(
    private uploadService: MediaUploadService,
    private basicService: BasicService,
    private tokenStorageService: TokenStorageService) {
  }

  private image: any;
  private defaultImage: string = "../../assets/avatars/default/img_avatar.png";
  private selectedFile: File;
  private user: User;

  ngOnInit() {
    this.getUserInfo();
    this.getUserPhoto();
  }

  selectFile(event) {
    this.selectedFile = event.target.files[0];
    this.upload();
  }



  getUserInfo() {
    this.basicService.getUserInfo(this.tokenStorageService.getKey()).subscribe(
      data => {
        this.user = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  getUserPhoto() {
    this.uploadService.getImage(this.tokenStorageService.getKey()).subscribe(
      data=>{
        this.image = data;
      }
    );

  }

  upload() {
    this.uploadService.changeImage(this.selectedFile,this.tokenStorageService.getKey()).subscribe(
      err => {
        console.log(err);
      }
    );
    this.getUserPhoto();
  }
}
