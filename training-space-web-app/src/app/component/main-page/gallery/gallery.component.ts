import {Component, OnInit} from '@angular/core';
import {BasicService} from "../../../service/basic.service";
import {TokenStorageService} from "../../../service/auth/token-storage.service";

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  constructor(private basicService: BasicService, private tokenStorageService: TokenStorageService) {
  }

  ngOnInit() {
  }

}
