import {Component, OnInit} from '@angular/core';
import {CourseService} from "../../../service/content/course.service";
import {Course} from "../../../model/Course";

@Component({
  selector: 'app-all-course',
  templateUrl: './all-course.component.html',
  styleUrls: [
    './all-course.component.css',
    '../gallery/gallery.component.css'
  ]
})
export class AllCourseComponent implements OnInit {
  private courses: Course[];

  constructor(
    private courseService: CourseService) {
  }

  ngOnInit() {
    this.courseService.getCourses().subscribe(
      response => {
        this.courses = response;
      },
      error => {
        console.log(error);
      }
    );
  }

  getImage(identity: string):string {
    return this.courseService.getImage(identity);
  }
}
