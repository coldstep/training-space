import {Component, OnInit} from '@angular/core';
import {Course} from "../../../model/Course";
import {CourseService} from "../../../service/content/course.service";

@Component({
  selector: 'app-top-course',
  templateUrl: './top-course.component.html',
  styleUrls: [
    './top-course.component.css',
    '../gallery/gallery.component.css'
  ]
})
export class TopCourseComponent implements OnInit {

  private courses: Course[];

  constructor(
    private courseService: CourseService) {
  }

  ngOnInit() {
    this.courseService.getCourses().subscribe(
      response => {
        this.courses = response;
      },
      error => {
        console.log(error);
      }
    );
  }

  getImage(identity: string):string {
    return this.courseService.getImage(identity);
  }
}
