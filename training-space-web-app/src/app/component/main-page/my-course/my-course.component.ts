import {Component, OnInit} from '@angular/core';
import {Course} from "../../../model/Course";
import {CourseService} from "../../../service/content/course.service";

@Component({
  selector: 'app-my-course',
  templateUrl: './my-course.component.html',
  styleUrls: [
    './my-course.component.css',
    '../gallery/gallery.component.css'
  ]
})
export class MyCourseComponent implements OnInit {

  private courses: Course[];

  constructor(
    private courseService: CourseService) {
  }

  ngOnInit() {
    this.courseService.getCourses().subscribe(
      response => {
        this.courses = response;
      },
      error => {
        console.log(error);
      }
    );
  }

  getImage(identity: string):string {
    return this.courseService.getImage(identity);
  }
}
