import {Component, OnInit} from '@angular/core';
import {SignUpInfo} from "../../../model/auth/SignUpInfo";
import {User} from "../../../model/User";
import {BasicService} from "../../../service/basic.service";

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  registrationUser: SignUpInfo;
  updateUser: User;
  users: User[];
  updateStatus: boolean = false;
  showNew: boolean = false;
  emptyPage: boolean;

  constructor(private basicService: BasicService) {

  }

  ngOnInit() {
    this.getAll();
    this.registrationUser = new SignUpInfo(
      '', '', '', '', ''
    );
  }

  private getAll(): void {
    this.basicService.getUsersInfo().subscribe(
      res => {
        this.users = res;
        (this.users.length != 0) ? this.emptyPage = true : this.emptyPage = false;
      },
      err => {
        this.emptyPage = false;
        // alert("Can't load your data !");
      }
    );
  }

  onEdit(user: User) {
    this.updateStatus = true;
    this.updateUser = user;
    this.registrationUser.username = user.username;
    this.registrationUser.firstName = user.firstName;
    this.registrationUser.lastName = user.lastName;
    this.registrationUser.email = user.email;
    this.onNew();
  }

  onDelete(user: User) {
    this.basicService.deleteUser(user).subscribe(
      data => {
        this.ngOnInit();
      },
      error => {
        alert(error);
      }
    );
  }

  onNew() {
    this.showNew = true;
  }

  onSave() {
    this.basicService.createUser(this.registrationUser).subscribe(
      data => {
        this.onCancel();
      },
      error => {
        alert(error);
      }
    );
  }

  onCancel() {
    this.showNew = false;
    this.ngOnInit()
  }

  openUserCourseList(user: User) {

  }
}
