import { Component, OnInit } from '@angular/core';
import {BasicService} from "../../../service/basic.service";
import {CourseService} from "../../../service/content/course.service";
import {VideoService} from "../../../service/content/video.service";
import {Course} from "../../../model/Course";

@Component({
  selector: 'app-course-management',
  templateUrl: './course-management.component.html',
  styleUrls: ['./course-management.component.css']
})
export class CourseManagementComponent implements OnInit {

  courses: Course[];
  emptyPage: boolean;

  constructor(private basicService: BasicService,
              private courseService: CourseService,
              private videoService: VideoService)
  { }

  ngOnInit() {
    this.getAll();
  }

  private getAll(): void {
    this.courseService.getCourses().subscribe(
      res => {
        this.courses = res;
        (this.courses.length != 0) ? this.emptyPage = true : this.emptyPage = false;
      },
      err => {
        alert("Can't load your data !");
      }
    );
  }

  removeCourse(identity: string): void {
    this.courseService.deleteCourse(identity).subscribe(
      data=>{
        this.ngOnInit();
      }
    );
  }
}
