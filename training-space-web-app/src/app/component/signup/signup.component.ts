import {Component, OnInit} from '@angular/core';
import {SignUpInfo} from "../../model/auth/SignUpInfo";
import {AuthService} from "../../service/auth/auth.service";
import {TokenStorageService} from "../../service/auth/token-storage.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form: any = {};
  signupInfo: SignUpInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';
  passwordConfirm: PasswordConfirm = {
    password: '',
    confirm_password: ''
  };

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService) {
  }

  ngOnInit() {
  }

  validate(): boolean {
    if (this.passwordConfirm.password == this.passwordConfirm.confirm_password) {
      this.form.password = this.passwordConfirm.password;
      return true;
    } else {
      return false;
    }
  }

  onSubmit() {

    this.signupInfo = new SignUpInfo(
      this.form.name,
      this.form.surname,
      this.form.username,
      this.form.email,
      this.form.password);

    this.authService.signUp(this.signupInfo).subscribe(
      data => {
        this.isSignedUp = true;
        this.isSignUpFailed = false;
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveIdentityKey(data.identityKey);
        // this.tokenStorage.saveAuthorities(data.authorities);
        (data) ? location.replace("/navigation") : alert("Rejected");
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}

export interface PasswordConfirm {
  password: string;
  confirm_password: string;
}
