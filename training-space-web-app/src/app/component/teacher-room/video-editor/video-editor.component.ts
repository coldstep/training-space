import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CourseService} from "../../../service/content/course.service";
import {VideoService} from "../../../service/content/video.service";
import {Video} from "../../../model/Video";
import {TokenStorageService} from "../../../service/auth/token-storage.service";

@Component({
  selector: 'app-video-editor',
  templateUrl: './video-editor.component.html',
  styleUrls: ['./video-editor.component.css']
})
export class VideoEditorComponent implements OnInit {

  video: Video = {
    uid: '',
    name: 'New video',
    videoSegmentUids: [''],
    videoType: '',
    createdDate: '',
    duration: '',
    description: "Write your description",
    courseUid: '',
    owner: ''
  };
  videoUrl: string;
  editorConfig: any = {
    "editable": true,
    "spellcheck": true,
    "height": "45vh",
    "minHeight": "40vh",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "placeholder": "Please enter text here...",
    "enableToolbar": true,
    "showToolbar": true,
    "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "strikeThrough"],
      ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"]
    ]
  };

  constructor(private route: ActivatedRoute, private router: Router, private storageService: TokenStorageService, private courseService: CourseService, private videoService: VideoService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      param => {
        if (param.get('uid') != 'new') {
          this.getVideoByCourse(param.get('uid'));
        }
      }
    );
  }

  private getVideoByCourse(identity: string): void {
    this.videoService.getVideo(identity).subscribe(
      data => {
        this.video = data;
      }
    );
    this.videoUrl = this.videoService.getVideoFile(identity);
  }

  saveVideoInfo() {
    if (this.video.uid.length == 0) {
      this.videoService.createVideo(this.video).subscribe(
        data => {
          this.video = data;
          this.router.navigate(['../../../../course-editor',this.video.courseUid], {relativeTo: this.route});
        }
      );
    } else {
      this.videoService.saveVideoInfo(this.video).subscribe(
        data => {
          this.video = data;
          this.router.navigate(['../../../../course-editor',this.video.courseUid], {relativeTo: this.route});
        }
      );
    }
  }

  updateFile(event) {
    const videoFile = event.target.files[0];
    if (this.video.uid.length == 0) {
      this.video.owner =  this.storageService.getKey();
      this.video.courseUid = this.route.pathFromRoot[3].snapshot.paramMap.get('uid');
      this.videoService.saveVideoFile(videoFile, this.video.owner, this.video.courseUid).subscribe(
        data => {
          this.video = data;
          this.getVideoByCourse(this.video.uid);
        }
      );
    } else {
      this.videoService.changeVideoFile(videoFile, this.video.uid, this.video.owner, this.video.courseUid).subscribe();
    }
  }
}
