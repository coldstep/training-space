import {Component, OnInit} from '@angular/core';
import {Video} from "../../../model/Video";
import {ActivatedRoute, Router} from "@angular/router";
import {CourseService} from "../../../service/content/course.service";
import {VideoService} from "../../../service/content/video.service";

@Component({
  selector: 'app-course-editor',
  templateUrl: './course-editor.component.html',
  styleUrls: ['./course-editor.component.css']
})
export class CourseEditorComponent implements OnInit {

  courseUid: string;
  videos: Video[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private courseService: CourseService,
    private videoService: VideoService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      param => {
        this.courseUid = param.get('uid');
        if (this.courseUid != 'new') {
          this.getVideosByCourse(this.courseUid);
          this.router.navigate(['./course', this.courseUid], {relativeTo: this.route});
        }
      }
    )
  }

  private getVideosByCourse(identity: string): void {
    this.videoService.getVideos(identity).subscribe(
      data => {
        this.videos = data;
      }
    )
  }

  removeVideo(id: string) {

  }
}
