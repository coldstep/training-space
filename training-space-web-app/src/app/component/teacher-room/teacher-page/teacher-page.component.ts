import {Component, OnInit} from '@angular/core';
import {BasicService} from "../../../service/basic.service";
import {CourseService} from "../../../service/content/course.service";
import {TokenStorageService} from "../../../service/auth/token-storage.service";
import {Course} from "../../../model/Course";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-teacher-page',
  templateUrl: './teacher-page.component.html',
  styleUrls: ['./teacher-page.component.css']
})
export class TeacherPageComponent implements OnInit {

  courses: Course[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private basicService: BasicService,
    private tokenStorageService: TokenStorageService,
    private courseService: CourseService,
  ) {
  }

  ngOnInit() {
    this.getTeacherCourses();
  }

  private getTeacherCourses(): void {
    this.courseService.getTeacherCourses(this.tokenStorageService.getKey()).subscribe(
      data => {
        this.courses = data;
        if (this.courses.length!= 0) {
          this.router.navigate(['./course-editor', this.courses[0].uid], {relativeTo: this.route});
        }
      }
    )
  }

  removeCourse(uid: string) {

  }

  publishCourse(courseUid: string) {

  }
}
