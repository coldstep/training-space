import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CourseService} from "../../../service/content/course.service";
import {Course} from "../../../model/Course";
import {TokenStorageService} from "../../../service/auth/token-storage.service";

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  course: Course = {
    uid: '',
    name: 'New course',
    description: 'Here write your course description',
    duration: '',
    owner: '',
    videos: ['']
  };
  image: any;

  editorConfig: any = {
    "editable": true,
    "spellcheck": true,
    "height": "45vh",
    "minHeight": "40vh",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "placeholder": "Please enter text here...",
    "enableToolbar": true,
    "showToolbar": true,
    "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "strikeThrough"],
      ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"]
    ]
  };


  constructor(private route: ActivatedRoute, private  router: Router, private courseService: CourseService, private storageService: TokenStorageService) {
  }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('uid') != 'new') {
      this.getCourseInfo(this.route.snapshot.paramMap.get('uid'));
    }
  }

  private getCourseInfo(uid: string): void {
    this.courseService.getCourse(uid).subscribe(
      data => {
        this.course = data;
        this.image = this.courseService.getImage(uid);
      }
    );

  }

  saveCourseInfo() {
    if (this.course.uid.length == 0) {
      this.course.owner = this.storageService.getKey();
      this.courseService.createCourse(this.course).subscribe(
        data => {
          this.course = data;
          this.courseService.saveImage(this.image, this.course).subscribe();
          this.router.navigate(['../../../../course-editor',this.course.uid], {relativeTo: this.route});
        }
      );
    } else {
      this.courseService.updateCourse(this.course).subscribe(
        data => {
          this.course = data;
        }
      )
      this.courseService.updateImage(this.image, this.course).subscribe(
        ok => {
          window.location.reload();
        }
      );

    }
  }

  changePhoto(event) {
    this.image = event.target.files[0];
  }

}
