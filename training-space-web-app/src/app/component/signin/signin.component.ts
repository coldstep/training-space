import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from "../../service/auth/token-storage.service";
import {SignInInfo} from "../../model/auth/SignInInfo";
import {AuthService} from "../../service/auth/auth.service";


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SignInComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  private signInInfo: SignInInfo;

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService) {
  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      // this.roles = this.tokenStorage.getAuthorities();
    }
  }

  onSubmit() {
    this.signInInfo = new SignInInfo(
      this.form.username,
      this.form.password);

    this.authService.attemptAuth(this.signInInfo).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveIdentityKey(data.identityKey);
        // this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        // this.roles = this.tokenStorage.getAuthorities();

        location.replace('/navigation');
      },
      error => {
        alert(error);
        this.errorMessage = error.error.message;
        this.isLoginFailed = true;
      }
    );
  }
}
