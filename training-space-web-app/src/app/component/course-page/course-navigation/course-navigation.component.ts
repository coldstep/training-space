import {Component, OnInit} from '@angular/core';
import {Course} from "../../../model/Course";
import {Video} from "../../../model/Video";
import {CourseService} from "../../../service/content/course.service";
import {VideoService} from "../../../service/content/video.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-course-navigation',
  templateUrl: './course-navigation.component.html',
  styleUrls: ['./course-navigation.component.css']
})
export class CourseNavigationComponent implements OnInit {
  course: Course;
  videos: Video[];
  image: string;

  constructor(private route: ActivatedRoute, private courseService: CourseService, private videoService: VideoService) {
  }

  ngOnInit() {
    this.getCourseData()
  }

  private getCourseData(): void {
    const uid = this.route.snapshot.paramMap.get('uid');
    this.courseService.getCourse(uid).subscribe(
      response => {
        this.course = response;
      },
      error => {
        console.log(error);
      });
    this.videoService.getVideos(uid).subscribe(
      data => {
        console.log(data);
        this.videos = data;
      }
    );
  }

  private getImage(uid: string) {
    return this.courseService.getImage(uid)
  }

  private getVideoImage(uid: string) {
    return this.videoService.getImage(uid)
  }
}

