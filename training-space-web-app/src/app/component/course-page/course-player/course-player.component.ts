import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {VideoService} from "../../../service/content/video.service";
import {Video} from "../../../model/Video";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-course-player',
  templateUrl: './course-player.component.html',
  styleUrls: ['./course-player.component.css']
})
export class CoursePlayerComponent implements OnInit {
  video: Video;
  videoUrl: string;
  @ViewChild('videoElement') videoElement: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private videoService: VideoService) {
  }

  ngOnInit() {
    this.getVideoData();
  }

  private getVideoData() {
    this.route.paramMap.pipe(
      switchMap(
        params => params.getAll('uid')
      )
    ).subscribe(route => {
      this.videoService.getVideo(route).subscribe(
        data => {
          this.video = data
        });
      this.videoUrl = this.videoService.getVideoFile(route);
    });
    // console.log(this.videoUrl);
  }

}
