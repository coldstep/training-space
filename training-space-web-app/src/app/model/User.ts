export interface User {
  uid: string;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  createdDate: string;
  description: string;
  role: string;
}
