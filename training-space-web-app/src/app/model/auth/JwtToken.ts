export interface JwtToken {
  token: string;
  identityKey: string;
}
