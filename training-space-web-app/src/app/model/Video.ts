export interface Video {
  uid: string;
  name: string;
  videoSegmentUids: string[];
  videoType: string;
  createdDate: string;
  duration: string;
  description: string;
  courseUid: string;
  owner: string;
}
