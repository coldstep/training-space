export interface Course {
  uid: string;
  name: string;
  description: string;
  duration: string;
  owner: string;
  videos: string[];
}
