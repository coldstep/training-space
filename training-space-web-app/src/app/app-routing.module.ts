import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from "./component/signin/signin.component";
import {SignupComponent} from "./component/signup/signup.component";
import {NavigationComponent} from "./component/navigation/navigation.component";
import {GalleryComponent} from "./component/main-page/gallery/gallery.component";
import {HistoryCourseComponent} from "./component/main-page/history-course/history-course.component";
import {MyCourseComponent} from "./component/main-page/my-course/my-course.component";
import {AllCourseComponent} from "./component/main-page/all-course/all-course.component";
import {TopCourseComponent} from "./component/main-page/top-course/top-course.component";
import {AdminNavigationComponent} from "./component/admin-page/admin-navigation/admin-navigation.component";
import {DashboardComponent} from "./component/admin-page/dashboard-components/dashboard/dashboard.component";
import {UserManagementComponent} from "./component/admin-page/user-management/user-management.component";
import {CourseManagementComponent} from "./component/admin-page/course-management/course-management.component";
import {DataManagementComponent} from "./component/admin-page/data-management/data-management.component";
import {TeacherPageComponent} from "./component/teacher-room/teacher-page/teacher-page.component";
import {CourseEditorComponent} from "./component/teacher-room/course-editor/course-editor.component";
import {CourseComponent} from "./component/teacher-room/course/course.component";
import {VideoEditorComponent} from "./component/teacher-room/video-editor/video-editor.component";
import {AboutComponent} from "./component/about/about.component";
import {AccountComponent} from "./component/account/account.component";
import {CourseNavigationComponent} from "./component/course-page/course-navigation/course-navigation.component";
import {CoursePlayerComponent} from "./component/course-page/course-player/course-player.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: 'full'
  },
  {
    path: 'navigation',
    component: NavigationComponent,
    children: [
      {
        path: '',
        redirectTo: 'gallery',
        pathMatch: 'full'
      },
      {
        path: 'gallery',
        component: GalleryComponent,
        children: [
          {
            path: '',
            redirectTo: 'top',
            pathMatch: 'full'
          },
          {
            path: 'top',
            component: TopCourseComponent
          },
          {
            path: 'all',
            component: AllCourseComponent
          },
          {
            path: 'my',
            component: MyCourseComponent
          },
          {
            path: 'history',
            component: HistoryCourseComponent
          },
          {
            path: 'course/:uid',
            component: CourseNavigationComponent,
            children: [
              {
                path: 'video/:uid',
                component: CoursePlayerComponent
              }
            ]
          }
        ]
      },
      {
        path: "teacher",
        component: TeacherPageComponent,
        children: [
          {
            path: 'course-editor/:uid',
            component: CourseEditorComponent,
            children: [
              {
                path: 'course/:uid',
                component: CourseComponent
              },
              {
                path: 'video/:uid',
                component: VideoEditorComponent
              }
            ]
          }
        ]
      },
      {
        path: 'account',
        component: AccountComponent
      },
      {
        path: 'about',
        component: AboutComponent
      }
    ]
  },
  {
    path: 'admin',
    component: AdminNavigationComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'user-management',
        component: UserManagementComponent
      },
      {
        path: 'course-management',
        component: CourseManagementComponent
      },
      {
        path: 'data-management',
        component: DataManagementComponent
      },
      {
        path: 'about',
        component: AboutComponent
      }
    ]
  },
  {
    path: 'sign-in',
    component: SignInComponent
  },
  {
    path: 'sign-up',
    component: SignupComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
