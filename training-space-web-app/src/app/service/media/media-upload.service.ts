import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Video} from "../../model/Video";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MediaUploadService {

  private URL = "http://localhost:8080/api/users/image";

  constructor(private http: HttpClient) {
  }

  getImage(userUid: any): Observable<any> {
    return this.http.get(`${this.URL}/${userUid}`);
  }

  changeImage(file: File, userUid: string): Observable<Video> {
    const formdata: FormData = new FormData();

    formdata.append('file', file);
    formdata.append('userUid', userUid)

    return this.http.post<Video>(this.URL, formdata);
  }
}
