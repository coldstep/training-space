import {Injectable} from '@angular/core';

import {Observable} from "rxjs";
import {User} from "../model/User";
import {HttpClient} from "@angular/common/http";
import {SignUpInfo} from "../model/auth/SignUpInfo";

@Injectable({
  providedIn: 'root'
})
export class BasicService {
  private BASE_URL = "http://localhost:8080/api/";
  private ADMIN_URL = "http://localhost:8080/api/admin/"
  private GET_USER = `${this.BASE_URL}users`;
  private DELETE_USER = `${this.ADMIN_URL}users/`;
  private CREATE_USER = `${this.ADMIN_URL}users`;

  constructor(private http: HttpClient) {
  }

  getUserInfo(identity: string):Observable<User>{
    return this.http.get<User>(`${this.GET_USER}/${identity}`);
  }

  getUsersInfo(): Observable<User[]>{
    return this.http.get<User[]>(this.GET_USER);
  }

  deleteUser(user: User):Observable<any> {
    return this.http.delete(this.DELETE_USER + user.uid);
  }

  createUser(signUpInfo: SignUpInfo):Observable<any> {
    return this.http.post(this.CREATE_USER, signUpInfo);
  }
}
