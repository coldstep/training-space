import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Video} from "../../model/Video";

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  private BASE_URL = "http://localhost:8082/api/course";
  private VIDEO = `${this.BASE_URL}/video`;
  private VIDEOS = `${this.BASE_URL}`;
  private VIDEO_FILE = `${this.BASE_URL}/video/file`;
  private IMAGE = `${this.BASE_URL}/video/image`;

  constructor(private http: HttpClient) {
  }

  getVideos(courseUid: string): Observable<Video[]> {
    return this.http.get<Video[]>(`${this.VIDEOS}/${courseUid}/video`);
  }

  getVideo(uid: string): Observable<Video> {
    return this.http.get<Video>(`${this.VIDEO}/${uid}`);
  }

  getVideoFile(uid: string): string {
    return `${this.VIDEO_FILE}/${uid}`;
  }

  getImage(identity: string) {
    return `${this.IMAGE}/${identity}`;
  }

  saveVideoInfo(video: Video): Observable<Video> {
    return this.http.put<Video>(this.VIDEO, video);
  }

  createVideo(video: Video): Observable<Video> {
    return this.http.post<Video>(this.VIDEO, video);
  }

  saveVideoFile(file: File, userUid: string, courseUid: string): Observable<Video> {
    const formdata: FormData = new FormData();

    formdata.append('file', file);
    formdata.append('userUid', userUid)
    formdata.append('courseUid', courseUid)

    return this.http.post<Video>(this.VIDEO_FILE,formdata);
  }

  changeVideoFile(file: File, videoUid: string, userUid: string, courseUid: string): Observable<any> {
    const formdata: FormData = new FormData();

    formdata.append('file', file);
    formdata.append('videoUid', videoUid)
    formdata.append('userUid', userUid)
    formdata.append('courseUid', courseUid)


    return this.http.put(this.VIDEO_FILE,formdata);
  }
}
