import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Course} from "../../model/Course";

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private BASE_URL = "http://localhost:8081/api/course";
  private GET_COURSE_LIST = `${this.BASE_URL}`;
  private GET_COURSE = `${this.BASE_URL}`;
  private GET_COURSE_BY_TEACHER = `${this.BASE_URL}/user/`
  private IMAGE = `${this.BASE_URL}/image`;
  private UPDATE_COURSE = this.BASE_URL;
  private SAVE_IMAGE = this.BASE_URL;
  private DELETE_COURSE = `${this.BASE_URL}`;
  private CREATE_COURSE = this.BASE_URL;

  constructor(private http: HttpClient) {
  }

  getCourse(identity: string): Observable<Course> {
    return this.http.get<Course>(`${this.GET_COURSE}/${identity}`);
  }

  getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(this.GET_COURSE_LIST);
  }

  getTeacherCourses(identity: string): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.GET_COURSE_BY_TEACHER}${identity}`);
  }

  getTopCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(this.GET_COURSE_LIST);
  }

  getUserHistory(): Observable<Course[]> {
    return this.http.get<Course[]>(this.GET_COURSE_LIST);
  }

  getUserCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(this.GET_COURSE_LIST);
  }

  createCourse(course: Course): Observable<Course> {
    return this.http.post<Course>(this.CREATE_COURSE, course);
  }

  updateCourse(course: Course): Observable<Course> {
    return this.http.put<Course>(this.UPDATE_COURSE, course);
  }

  deleteCourse(identity: string): Observable<any> {
    return this.http.delete(`${this.DELETE_COURSE}/${identity}`);
  }

  getImage(identity: string) {
    return `${this.IMAGE}/${identity}`;
  }

  saveImage(file: File, course: Course): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();

    formdata.append('file', file);
    formdata.append('userUid', course.owner)
    formdata.append('courseUid', course.uid)

    const req = new HttpRequest('POST', this.IMAGE, formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  updateImage(file: File, course: Course): Observable<HttpEvent<{}>> {

    const formdata: FormData = new FormData();

    formdata.append('file', file);
    formdata.append('userUid', course.owner)
    formdata.append('courseUid', course.uid)

    const req = new HttpRequest('PUT', this.IMAGE, formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }
}

