import {Injectable} from '@angular/core';

const TOKEN_KEY = 'AuthToken';
const IDENTITY_KEY = 'IdentityKey';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  private roles: Array<string> = [];

  constructor() {
  }

  public signOut() {
    window.sessionStorage.clear();
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public saveIdentityKey(key: string) {
    window.sessionStorage.removeItem(key);
    window.sessionStorage.setItem(IDENTITY_KEY, key);
  }

  public getKey(): string {
    return sessionStorage.getItem(IDENTITY_KEY);
  }

  // public saveAuthorities(authorities: string[]) {
  //   window.sessionStorage.removeItem(AUTHORITIES_KEY);
  //   window.sessionStorage.setItem(AUTHORITIES_KEY, JSON.stringify(authorities));
  // }

  // public getAuthorities(): string[] {
  //   this.roles = [];
  //
  //   if (sessionStorage.getItem(TOKEN_KEY)) {
  //     JSON.parse(sessionStorage.getItem(AUTHORITIES_KEY)).forEach(authority => {
  //       this.roles.push(authority.authority);
  //     });
  //   }
  //
  //   return this.roles;
  // }
}
