import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {SignInInfo} from "../../model/auth/SignInInfo";
import {JwtToken} from "../../model/auth/JwtToken";
import {SignUpInfo} from "../../model/auth/SignUpInfo";
import {User} from "../../model/User";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private PORT = '8080'
  private SIGN_IN = `http://localhost:${this.PORT}/api/auth/signin`;
  private SIGN_UP = `http://localhost:${this.PORT}/api/auth/signup`;
  private BASE_URL = "http://localhost:8080/api/users";

  constructor(private http: HttpClient) {

  }

  attemptAuth(credentials: SignInInfo): Observable<JwtToken> {
    return this.http.post<JwtToken>(this.SIGN_IN, credentials, httpOptions);
  }

  signUp(info: SignUpInfo): Observable<JwtToken> {
    return this.http.post<JwtToken>(this.SIGN_UP, info, httpOptions);
  }
}
