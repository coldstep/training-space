import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SignInComponent} from './component/signin/signin.component';
import {HttpClientModule} from "@angular/common/http";
import { SignupComponent } from './component/signup/signup.component';
import {FormsModule} from "@angular/forms";
import { NavigationComponent } from './component/navigation/navigation.component';
import { GalleryComponent } from './component/main-page/gallery/gallery.component';
import {httpInterceptorProviders} from "./service/auth/auth-interceptor";
import { TopCourseComponent } from './component/main-page/top-course/top-course.component';
import { AllCourseComponent } from './component/main-page/all-course/all-course.component';
import { MyCourseComponent } from './component/main-page/my-course/my-course.component';
import { HistoryCourseComponent } from './component/main-page/history-course/history-course.component';
import { AdminNavigationComponent } from './component/admin-page/admin-navigation/admin-navigation.component';
import { DashboardComponent } from './component/admin-page/dashboard-components/dashboard/dashboard.component';
import { UserManagementComponent } from './component/admin-page/user-management/user-management.component';
import { CourseManagementComponent } from './component/admin-page/course-management/course-management.component';
import {ChartsModule} from "ng2-charts";
import { UserActivityComponent } from './component/admin-page/dashboard-components/user-activity/user-activity.component';
import { UserStatisticComponent } from './component/admin-page/dashboard-components/user-statistic/user-statistic.component';
import { RegistrationStatisticComponent } from './component/admin-page/dashboard-components/registration-statistic/registration-statistic.component';
import { DataManagementComponent } from './component/admin-page/data-management/data-management.component';
import { TeacherPageComponent } from './component/teacher-room/teacher-page/teacher-page.component';
import { CourseEditorComponent } from './component/teacher-room/course-editor/course-editor.component';
import { CourseComponent } from './component/teacher-room/course/course.component';
import { VideoEditorComponent } from './component/teacher-room/video-editor/video-editor.component';
import { AboutComponent } from './component/about/about.component';
import {AccountComponent} from "./component/account/account.component";
import { CoursePlayerComponent } from './component/course-page/course-player/course-player.component';
import { CourseNavigationComponent } from './component/course-page/course-navigation/course-navigation.component';
import {NgxEditorModule} from "ngx-editor";

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignupComponent,
    NavigationComponent,
    GalleryComponent,
    TopCourseComponent,
    AllCourseComponent,
    MyCourseComponent,
    HistoryCourseComponent,
    AdminNavigationComponent,
    DashboardComponent,
    UserManagementComponent,
    CourseManagementComponent,
    UserActivityComponent,
    UserStatisticComponent,
    RegistrationStatisticComponent,
    DataManagementComponent,
    TeacherPageComponent,
    CourseEditorComponent,
    CourseComponent,
    VideoEditorComponent,
    AboutComponent,
    AccountComponent,
    CoursePlayerComponent,
    CourseNavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
    NgxEditorModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
