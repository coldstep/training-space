package com.vbproduct.trainingspacecoreservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Table(name = "user")
@NoArgsConstructor
public class User {

    @Id
    private String uid;
    private String username;
    private String password;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    private String email;
    @Column(name =  "created_date")
    private LocalDate createdDate;
    private String description;
    private RoleName role;
    private String image;
}
