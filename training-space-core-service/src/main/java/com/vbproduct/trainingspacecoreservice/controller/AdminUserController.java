package com.vbproduct.trainingspacecoreservice.controller;

import com.vbproduct.trainingspacecoreservice.dto.RegisterDto;
import com.vbproduct.trainingspacecoreservice.dto.UserDto;
import com.vbproduct.trainingspacecoreservice.dto.converter.DtoConverter;
import com.vbproduct.trainingspacecoreservice.model.User;
import com.vbproduct.trainingspacecoreservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/admin/users")
public class AdminUserController {

    @Autowired
    private UserService userService;

    @GetMapping()
    public List<User> getUsers() {
        var users = userService.findAll();
        if (users.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Any users was found");
        }
        return users;
    }

    @GetMapping("/{userUid}")
    public UserDto getUserByUid(@PathVariable String userUid) {
        return DtoConverter.toUserDto(userService.findUserByUid(userUid)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")));
    }

    @PutMapping
    public UserDto update(@RequestBody UserDto userDto) {
        return DtoConverter.toUserDto(userService.update(DtoConverter.toUserEntity(userDto)));
    }

    @PostMapping
    public UserDto createUser(@RequestBody RegisterDto registerDto) {
        return DtoConverter.toUserDto(userService.createUser(DtoConverter.toUserFromRegisterDto(registerDto)));
    }

    @DeleteMapping("/{uid}")
    public void deleteUser(@PathVariable String uid) {
        userService.deleteUser(uid);
    }
}
