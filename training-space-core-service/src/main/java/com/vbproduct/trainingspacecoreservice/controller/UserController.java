package com.vbproduct.trainingspacecoreservice.controller;


import com.vbproduct.trainingspacecoreservice.dto.UserDto;
import com.vbproduct.trainingspacecoreservice.dto.converter.DtoConverter;
import com.vbproduct.trainingspacecoreservice.model.User;
import com.vbproduct.trainingspacecoreservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.net.MalformedURLException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping()
    public List<User> getUsers() {
        var users = userService.findAll();
        if (users.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Any users was found");
        }
        return users;
    }

    @GetMapping("/{userUid}")
    public UserDto getUserByUid(@PathVariable String userUid) {
        return DtoConverter.toUserDto(userService.findUserByUid(userUid)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")));
    }

    @PutMapping
    public UserDto update(@RequestBody UserDto userDto) {
        return DtoConverter.toUserDto(userService.update(DtoConverter.toUserEntity(userDto)));
    }

    @GetMapping("/image/{userUid}")
    public ResponseEntity<String> getUserImage(@PathVariable String userUid) throws MalformedURLException {
        var image = userService.getImage(userUid);
        return ResponseEntity.ok().body(image);
    }


    @PostMapping("image")
    public void saveImage(@RequestParam String userUid, @RequestParam("file") MultipartFile multipartFile) {
        userService.saveImage(userUid, multipartFile);
    }
}
