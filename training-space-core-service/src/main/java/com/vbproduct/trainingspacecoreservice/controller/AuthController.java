package com.vbproduct.trainingspacecoreservice.controller;

import com.vbproduct.trainingspacecoreservice.dto.RegisterDto;
import com.vbproduct.trainingspacecoreservice.dto.converter.DtoConverter;
import com.vbproduct.trainingspacecoreservice.security.model.AuthenticationRequest;
import com.vbproduct.trainingspacecoreservice.security.model.AuthenticationResponse;
import com.vbproduct.trainingspacecoreservice.security.service.TSUSerDetailsService;
import com.vbproduct.trainingspacecoreservice.security.util.JwtUtil;
import com.vbproduct.trainingspacecoreservice.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TSUSerDetailsService tbsUserDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/signin")
    public AuthenticationResponse createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            var auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
            SecurityContextHolder.getContext().setAuthentication(auth);
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("Incorrect username or password", e);
        }
        UserDetails userDetails = tbsUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        String userUid = userService.findByUsername(userDetails.getUsername()).get().getUid();
        String jwtToken = jwtUtil.generateToken(userDetails);
        return new AuthenticationResponse(jwtToken, userUid);
    }

    @PostMapping("/signup")
    public AuthenticationResponse signUpUser(@RequestBody RegisterDto registerDto) {
        var user = userService.createUser(DtoConverter.toUserFromRegisterDto(registerDto));
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(registerDto.getUsername(), registerDto.getPassword())
            );
        } catch (Exception e) {
            log.info("Catch exception ", e);
        }
        UserDetails userDetails = tbsUserDetailsService.loadUserByUsername(user.getUsername());
        String jwtToken = jwtUtil.generateToken(userDetails);
        return new AuthenticationResponse(jwtToken, user.getUid());
    }

}
