package com.vbproduct.trainingspacecoreservice.dto.converter;

import com.vbproduct.trainingspacecoreservice.dto.RegisterDto;
import com.vbproduct.trainingspacecoreservice.dto.UserDto;
import com.vbproduct.trainingspacecoreservice.model.RoleName;
import com.vbproduct.trainingspacecoreservice.model.User;

public class DtoConverter {

    public static User toUserEntity(UserDto userDto) {
        var user = new User();
        user.setUid(userDto.getUid());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setDescription(userDto.getDescription());
        user.setRole(RoleName.valueOf(userDto.getRole()));
        return user;
    }

    public static UserDto toUserDto(User user) {
        var userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setCreatedDate(user.getCreatedDate());
        userDto.setDescription(user.getDescription());
        userDto.setEmail(user.getEmail());
        userDto.setRole(user.getRole().getAuthority());
        userDto.setUid(user.getUid());
        return userDto;
    }

    public static User toUserFromRegisterDto(RegisterDto registerDto) {
        var user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());
        user.setRole(RoleName.valueOf(registerDto.getRole()));
        return user;
    }

}
