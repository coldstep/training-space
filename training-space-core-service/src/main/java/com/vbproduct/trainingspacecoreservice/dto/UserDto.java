package com.vbproduct.trainingspacecoreservice.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UserDto {
    private String uid;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDate createdDate;
    private String description;
    private String role;
}
