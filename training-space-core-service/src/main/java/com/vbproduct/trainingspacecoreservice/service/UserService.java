package com.vbproduct.trainingspacecoreservice.service;


import com.vbproduct.trainingspacecoreservice.model.User;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> findUserByUid(String uid);

    Optional<User> findByUsername(String username);

    List<User> findAll();

    User createUser(User user);

    User update(User user);

    void deleteUser(String uid);

    String getImage(String userUid) throws MalformedURLException;

    void saveImage(String userUid, MultipartFile multipartFile);
}
