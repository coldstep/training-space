package com.vbproduct.trainingspacecoreservice.service.impl;


import com.vbproduct.trainingspacecoreservice.model.User;
import com.vbproduct.trainingspacecoreservice.repository.UserRepository;
import com.vbproduct.trainingspacecoreservice.service.UserService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Value("${training-space.dir-path}")
    private String dirPath;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Optional<User> findUserByUid(String uid) {
        return userRepository.findById(uid);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User createUser(User user) {
        if (!userRepository.existsByUsername(user.getUsername())) {
            user.setUid(UUID.randomUUID().toString());
            user.setCreatedDate(LocalDate.now());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.save(user);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User exist");
    }

    @Override
    public User update(User user) {
        //TODO change password ?
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(String uid) {
        userRepository.deleteById(uid);
    }

    @Override
    public String getImage(String userUid) throws MalformedURLException {
        var user = userRepository.findById(userUid).orElseThrow();
        String image = null;
        try {
            var bytes = FileUtils.readFileToByteArray(Paths.get(user.getImage()).toFile());
            var encodedToBase64 = Base64.getEncoder().encodeToString(bytes);
            image = ("[\"data:image/png" + ";base64," + encodedToBase64 + "\"]");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    public void saveImage(String userUid, MultipartFile file) {
        var user = userRepository.findById(userUid).orElseThrow();
        String tempPath = dirPath + userUid + "/" + "photo/" + file.getOriginalFilename();
        user.setImage(tempPath);
        try {
            FileUtils.writeByteArrayToFile(Paths.get(tempPath).toFile(), file.getBytes());
            userRepository.save(user);
        } catch (IOException | IllegalStateException e) {
            e.printStackTrace();
        }
    }
}
