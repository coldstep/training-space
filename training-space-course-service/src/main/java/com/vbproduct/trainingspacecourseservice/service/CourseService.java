package com.vbproduct.trainingspacecourseservice.service;

import com.vbproduct.trainingspacecourseservice.model.Course;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface CourseService {

    List<Course> getAllLibrary();

    Course getByUid(String courseUid);

    List<Course> getOwnerCourses(String ownerUid);

    Course createCourse(Course course);

    Course updateCourse(Course course);

    void deleteCourse(String courseUid);

    void deleteUserCourses(String ownerUid);

    Resource getImage(String courseUid) throws IOException;

    void saveImage(String userUid, String courseUid, MultipartFile file);

    void updateImage(String userUid, String courseUid, MultipartFile file);
}
