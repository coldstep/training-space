package com.vbproduct.trainingspacecourseservice.service.impl;

import com.vbproduct.trainingspacecourseservice.dto.convertor.DtoConverter;
import com.vbproduct.trainingspacecourseservice.model.Course;
import com.vbproduct.trainingspacecourseservice.repository.CourseRepository;
import com.vbproduct.trainingspacecourseservice.service.CourseService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class CourseServiceImpl implements CourseService {

    @Value("${training-space.dir-path}")
    private String dirPath;

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<Course> getAllLibrary() {
        return courseRepository.findAll();
    }

    @Override
    public Course getByUid(String courseUid) {
        return courseRepository.findById(courseUid).orElseThrow();
    }

    @Override
    public List<Course> getOwnerCourses(String ownerUid) {
        return courseRepository.findAllByOwnerUid(ownerUid);
    }

    @Override
    public Course createCourse(Course course) {
        course.setUid(UUID.randomUUID().toString());
        return courseRepository.save(course);
    }

    @Override
    public Course updateCourse(Course course) {
        var fromDB = courseRepository.findById(course.getUid()).orElseThrow();
        updateCourseFields(course,fromDB);
        return courseRepository.save(fromDB);
    }

    @Override
    public void deleteCourse(String courseUid) {
        courseRepository.deleteById(courseUid);
    }

    @Transactional
    @Override
    public void deleteUserCourses(String ownerUid) {
        courseRepository.deleteAllByOwnerUid(ownerUid);
    }

    @Override
    public Resource getImage(String courseUid) throws IOException {
        var course = courseRepository.findById(courseUid).orElseThrow();
        return new UrlResource(Paths.get(course.getImage()).toUri());
    }

    @Override
    public void saveImage(String userUid, String courseUid, MultipartFile file) {
        var course = courseRepository.findById(courseUid).orElseThrow();
        String tempPath = dirPath  + userUid + "/" + courseUid + "/" + "photo/" + file.getOriginalFilename();
        course.setImage(tempPath);
        try {
            FileUtils.writeByteArrayToFile(Paths.get(tempPath).toFile(), file.getBytes());
            courseRepository.save(course);
        } catch (IOException | IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateImage(String userUid, String courseUid, MultipartFile file) {
        File olderFile = null;
        var course = courseRepository.findById(courseUid).orElseThrow();
        var olderPath = course.getImage();
        if (olderPath != null) {
            olderFile = Paths.get(olderPath).toFile();
        }
        String tempPath = dirPath + userUid + "/" + courseUid + "/" + "photo/" + file.getOriginalFilename();
        try {
            if (olderFile!=null && olderFile.exists()) {
                olderFile.delete();
            }
            course.setImage(tempPath);
            courseRepository.save(course);
            FileUtils.writeByteArrayToFile(Paths.get(tempPath).toFile(), file.getBytes());
        } catch (IOException | IllegalStateException e) {
            e.printStackTrace();
        }
    }


    private void updateCourseFields(Course fromClient, Course fromDb){
        fromDb.setDescription(fromClient.getDescription());
        fromDb.setName(fromClient.getName());
        //TODO when edit videos then delete older
        fromDb.setVideos(fromClient.getVideos());
    }
}
