package com.vbproduct.trainingspacecourseservice.repository;

import com.vbproduct.trainingspacecourseservice.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, String> {

    List<Course> findAllByOwnerUid(String ownerUid);

    void deleteAllByOwnerUid(String ownerUid);
}
