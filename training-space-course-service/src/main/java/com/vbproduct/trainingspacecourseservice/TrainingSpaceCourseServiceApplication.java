package com.vbproduct.trainingspacecourseservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingSpaceCourseServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingSpaceCourseServiceApplication.class, args);
	}

}
