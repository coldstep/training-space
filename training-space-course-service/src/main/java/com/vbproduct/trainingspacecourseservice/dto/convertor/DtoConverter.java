package com.vbproduct.trainingspacecourseservice.dto.convertor;

import com.vbproduct.trainingspacecourseservice.dto.CourseDto;
import com.vbproduct.trainingspacecourseservice.model.Course;

import java.util.List;
import java.util.stream.Collectors;

public class DtoConverter {

    public static CourseDto toDto(Course course) {
        var courseDto = new CourseDto();
        courseDto.setUid(course.getUid());
        courseDto.setName(course.getName());
        courseDto.setDescription(course.getDescription());
        courseDto.setDuration(course.getDuration());
        courseDto.setOwner(course.getOwnerUid());
        courseDto.setVideos(course.getVideos());
        return courseDto;
    }

    public static Course toEntity(CourseDto courseDto) {
        var course = new Course();
        course.setUid(courseDto.getUid());
        course.setName(courseDto.getName());
        course.setDescription(courseDto.getDescription());
        course.setDuration(courseDto.getDuration());
        course.setOwnerUid(courseDto.getOwner());
        course.setVideos(courseDto.getVideos());
        return course;
    }

    public static List<CourseDto> toCourseDtoList(List<Course> courseList) {
        return courseList.stream().map(DtoConverter::toDto).collect(Collectors.toList());
    }
}
