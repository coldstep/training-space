package com.vbproduct.trainingspacecourseservice.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CourseDto {

    private String uid;
    private String name;
    private String description;
    private String duration;
    private String owner;
    private Set<String> videos;
//    private Set<String> tests;
//    private Set<String> lectures;
}
