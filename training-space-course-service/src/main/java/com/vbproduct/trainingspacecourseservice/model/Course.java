package com.vbproduct.trainingspacecourseservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "course")
public class Course {

    @Id
    private String uid;
    private String name;
    private String image;
    private String description;
    private String duration;
    @Column(name = "owner_uid",nullable = false)
    private String ownerUid;
    @Embedded
    @ElementCollection
    private Set<String> videos;
//    private List<> tests;
//    private List<> lectures;
}
