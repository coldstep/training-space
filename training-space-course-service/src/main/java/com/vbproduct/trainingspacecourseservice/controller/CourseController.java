package com.vbproduct.trainingspacecourseservice.controller;

import com.vbproduct.trainingspacecourseservice.dto.CourseDto;
import com.vbproduct.trainingspacecourseservice.dto.convertor.DtoConverter;
import com.vbproduct.trainingspacecourseservice.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/course")
public class CourseController {


    @Autowired
    private CourseService courseService;

    @GetMapping
    public List<CourseDto> getCourses() {
        return DtoConverter.toCourseDtoList(courseService.getAllLibrary());
    }

    @GetMapping("/user/{ownerUid}")
    public List<CourseDto> getCourseByOwner(@PathVariable String ownerUid) {
        return DtoConverter.toCourseDtoList(courseService.getOwnerCourses(ownerUid));
    }

    @GetMapping("/{courseUid}")
    public CourseDto getCourse(@PathVariable String courseUid) {
        return DtoConverter.toDto(courseService.getByUid(courseUid));
    }

    @PostMapping
    public CourseDto createCourse(@RequestBody CourseDto courseDto) {
        return DtoConverter.toDto(courseService.createCourse(DtoConverter.toEntity(courseDto)));
    }

    @PutMapping
    public CourseDto updateCourse(@RequestBody CourseDto courseDto) {
        return DtoConverter.toDto(courseService.updateCourse(DtoConverter.toEntity(courseDto)));
    }

    @DeleteMapping("/{courseUid}")
    public void deleteCourse(@PathVariable String courseUid) {
        courseService.deleteCourse(courseUid);
    }

    @GetMapping("image/{courseUid}")
    public ResponseEntity<Resource> getCourseImage(@PathVariable String courseUid) throws IOException {
        var image = courseService.getImage(courseUid);
        return ResponseEntity.ok().body(image);
    }

    @PostMapping("image")
    public void saveImage(@RequestParam String userUid, @RequestParam String courseUid, @RequestParam("file") MultipartFile multipartFile) {
        courseService.saveImage(userUid, courseUid, multipartFile);
    }

    @PutMapping("image")
    public void changeImage(@RequestParam String userUid, @RequestParam String courseUid, @RequestParam("file") MultipartFile multipartFile) {
        courseService.updateImage(userUid, courseUid, multipartFile);
    }
}
